//
//  AppDelegate.swift
//  PFLoginTest
//
//  Created by Alex Macleod on 15/3/15.
//  Copyright (c) 2015 Alex Macleod. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        Parse.setApplicationId("4JeL2IYVi4r04xLPtaLVMTt2BmgnfD9XWWW46PyM", clientKey: "viT6GXlftHes7mV6HQ1ishVrPQEoNuJaqkdRx8dE")
        PFAnalytics.trackAppOpenedWithLaunchOptionsInBackground(launchOptions, block: nil)
 
        PFFacebookUtils.initializeFacebook()
//        var testObject: PFObject = PFObject(className: "TestObject")
//        testObject["foo"] = "bar"
//        testObject.saveInBackgroundWithBlock(nil)
        
        // Set default ACLs
//        var defaultACL = PFACL.ACL as PFACL()
//        defaultACL().setPublicReadAccess(true)
//        PFACL.setDefaultACL(defaultACL, withAccessForCurrentUser: true)
//        PFACL *defaultACL = [PFACL ACL];
//        [defaultACL setPublicReadAccess:YES];
//        [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];

        return true
    }

    func application(application: UIApplication, handleOpenURL url: NSURL) -> Bool {
        return FBAppCall.handleOpenURL(url, sourceApplication: "PFLoginTest")
//        handleOpenURL:url
//            sourceApplication:sourceApplication
//            withSession:[PFFacebookUtils session]];        return handleOpenURL
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        return FBAppCall.handleOpenURL(url, sourceApplication: "PFLoginTest")
    }
//    // Facebook oauth callback
//    - (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
//    return [PFFacebookUtils handleOpenURL:url];
//    }
//    
//    - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
//    sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
//    return [PFFacebookUtils handleOpenURL:url];
//    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        FBSession.activeSession().handleDidBecomeActive()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

