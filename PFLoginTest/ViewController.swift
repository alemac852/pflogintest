//
//  ViewController.swift
//  PFLoginTest
//
//  Created by Alex Macleod on 15/3/15.
//  Copyright (c) 2015 Alex Macleod. All rights reserved.
//

import UIKit

class ViewController: UIViewController, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate {

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if (PFUser.currentUser() == nil) {
            
            var logInViewController = PFLogInViewController()
            
            logInViewController.delegate = self
            
            logInViewController.fields = PFLogInFields.UsernameAndPassword | PFLogInFields.LogInButton | PFLogInFields.SignUpButton | PFLogInFields.PasswordForgotten | PFLogInFields.Facebook
            
            logInViewController.facebookPermissions = NSArray(array: ["user_about_me"])
            println("logInViewController.facebookPermissions: - \(logInViewController.facebookPermissions)")
//            [logInViewController setFacebookPermissions:[NSArray arrayWithObjects:@"friends_about_me", nil]];
            var signUpViewController = PFSignUpViewController()
            
            signUpViewController.delegate = self
            
            logInViewController.signUpController = signUpViewController
            
            self.presentViewController(logInViewController, animated: true, completion: nil)
            
        } else {
            
            label.text = PFUser.currentUser().username as NSString
//            label.text = PFUser.currentUser()
//            if !PFUser.currentUser().email.isEmpty {
//                label.text = PFUser.currentUser().email as NSString
//            }
//            label.text = PFUser.currentUser().email as NSString
        }

    }

    // MARK: - Parse Login
    
    func logInViewController(logInController: PFLogInViewController!, shouldBeginLogInWithUsername username: String!, password: String!) -> Bool {
        
        if (!username.isEmpty || !password.isEmpty) {
            return true
        }else {
            return false
        }
        
        
    }
    var facebookAge:Int!

    func logInViewController(logInController: PFLogInViewController!, didLogInUser user: PFUser!) {
        
        FBRequestConnection.startForMeWithCompletionHandler { (connection:FBRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
            if error ==  nil {
                println("result: - \(result)")
                
                //Finding age of user
                var facebookBirthday = result.objectForKey("birthday") as String
                println("facebookBirthday: - \(facebookBirthday)")
                
                var dateFormatter:NSDateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM-dd-yyyy"
                var currentDate = dateFormatter.stringFromDate(NSDate()) as String 
                println("currentDate: - \(currentDate)")
                
                var facebookMonth = facebookBirthday[0..<2].toInt()
                var facebookDay = facebookBirthday[3..<5].toInt()
                var facebookYear = facebookBirthday[6..<10].toInt()
                println("facebookMonth: - \(facebookMonth)")
                println("facebookDay: - \(facebookDay)")
                println("facebookYear: - \(facebookYear)")
                
                var currentDateMonth = currentDate[0..<2].toInt()
                var currentDateDay = currentDate[3..<5].toInt()
                var currentDateYear = currentDate[6..<10].toInt()
                println("currentDateMonth: - \(currentDateMonth)")
                println("currentDateDay: - \(currentDateDay)")
                println("currentDateYear: - \(currentDateYear)")

                var ageFromYear = currentDateYear! - facebookYear!
                // Found age of user
                if currentDateMonth <= facebookMonth && currentDateDay < facebookDay {
                    self.facebookAge = ageFromYear - 1
                    println("facebookAge: - \(self.facebookAge)")
                }
                
                var facebookEmail = result.objectForKey("email") as String
                var facebookId = result.objectForKey("id") as String
                var facebookLink = result.objectForKey("link") as String
                
                var facebookLocationData = result.objectForKey("location") as NSDictionary
                var facebookLocationId = facebookLocationData["id"] as String
                var facebookLocationName = facebookLocationData["name"] as String
                
                var facebookName = result.objectForKey("name") as String
                var facebookGender = result.objectForKey("gender") as String
                //Stroing info in parse
                if !facebookBirthday.isEmpty && !facebookBirthday.isEmpty {
                    PFUser.currentUser().setObject(facebookBirthday, forKey: "facebookBirthday")
                }
                
                if (self.facebookAge != nil) {
                    PFUser.currentUser().setObject(self.facebookAge, forKey: "facebookAge")
                }

                if !facebookEmail.isEmpty && !facebookEmail.isEmpty {
                    PFUser.currentUser().setObject(facebookEmail, forKey: "facebookEmail")
                }

                if !facebookId.isEmpty && !facebookId.isEmpty {
                    PFUser.currentUser().setObject(facebookId, forKey: "facebookId")
                }

                if !facebookLink.isEmpty && !facebookLink.isEmpty {
                    PFUser.currentUser().setObject(facebookLink, forKey: "facebookLink")
                }
                
                if !facebookLocationId.isEmpty && !facebookLocationId.isEmpty {
                    PFUser.currentUser().setObject(facebookLocationId, forKey: "facebookLocationId")
                }

                if !facebookLocationName.isEmpty && !facebookLocationName.isEmpty {
                    PFUser.currentUser().setObject(facebookLocationName, forKey: "facebookLocationName")
                }

                if !facebookName.isEmpty && !facebookName.isEmpty {
                    PFUser.currentUser().setObject(facebookName, forKey: "facebookName")
                }
                
                if !facebookGender.isEmpty && !facebookGender.isEmpty {
                    PFUser.currentUser().setObject(facebookGender, forKey: "facebookGender")
                }
                
                println("facebookId + facebookName: - \(facebookId + facebookName)")
                
                PFUser.currentUser().saveInBackgroundWithBlock({ (succeded:Bool, error:NSError!) -> Void in
                    println("saveInBackgroundWithBlock: - \(error)")
                    if error == nil {
                        FBRequestConnection.startForMyFriendsWithCompletionHandler({ (connection:FBRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
                            println("startForMyFriendsWithCompletionHandler: - \(error)")

                            if error == nil {
                                var data:NSArray = result.objectForKey("data") as NSArray
                                var facebookIds = NSMutableArray(capacity: data.count)
//                                var friendData = NSDictionary()
                                for friendData in data {
                                    facebookIds.addObject(friendData.objectForKey("id")!)
                                    
                                    println("facebookIds: - \(facebookIds)")
                                }
                                
                                PFUser.currentUser().setObject(facebookIds, forKey: "facebookFriends")
                                PFUser.currentUser().saveInBackgroundWithBlock({ (succeded:Bool, error:NSError!) -> Void in
                                    
                                    if error == nil {
                                        self.dismissViewControllerAnimated(true, completion: nil)
                                    }
                                })
                            }
                        })
                    }
                })
            }
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func logInViewController(logInController: PFLogInViewController!, didFailToLogInWithError error: NSError!) {
        println("Failed to log in...")
    }
    
    func signUpViewController(signUpController: PFSignUpViewController!, shouldBeginSignUp info: [NSObject : AnyObject]!) -> Bool {
        
        if let password = info?["password"] as? String {
            return password.utf16Count >= 8
        }
        return false
        
    }
    
    func signUpViewController(signUpController: PFSignUpViewController!, didSignUpUser user: PFUser!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func signUpViewController(signUpController: PFSignUpViewController!, didFailToSignUpWithError error: NSError!) {
        println("Failed to sign up...")
    }
    
    func signUpViewControllerDidCancelSignUp(signUpController: PFSignUpViewController!) {
        println("User dismissed sign up.")
    }

    @IBAction func logout(sender: AnyObject) {
        PFUser.logOut()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension String {
    
    subscript (i: Int) -> Character {
        return self[advance(self.startIndex, i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        return substringWithRange(Range(start: advance(startIndex, r.startIndex), end: advance(startIndex, r.endIndex)))
    }
}

